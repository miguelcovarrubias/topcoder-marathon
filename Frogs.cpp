// C++17
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <iomanip>
using namespace std;
#define mp make_pair
#define f first
#define s second
#define pb push_back
#define in(x,n) (x>=0 && x<n)
#define lim 31
#define p(x) ;//cerr << #x << ":" << x << "\n"

int H;
int W;
char grid[lim][lim];
char rowDir[lim];
int dx[] = {-1, 0, 1, 0,0};
int dy[] = {0, 1, 0,-1,0};
char dir[] = "URDL";
vector<vector<pair<int, int>>> D;
int T[lim][lim];

void printGrid(vector<vector<pair<int,int>>> M){
  return;
  for (int i = 0; i < H;i++){
    for (int j = 0; j < W;j++)
        cerr << setw(2) << M[i][j].f<<","<<setw(2)<<M[i][j].s << " ";
    cerr << "\n";
  }
  cerr << "\n";
}
vector<vector<pair<int,int>>> bfs(int r,int c,int t0)
{
  vector<vector<pair<int,int>>> D(H);
  for (int i = 0; i < H;i++)
    for (int j = 0; j < W;j++)
      D[i].pb(mp(-1,-1));
  D[r][c].f = t0;
  queue<pair<int, int>> Q;
  Q.push(mp(r, c));
  p(t0);
  int cnt = 0;
  while (!Q.empty())
  {
    int x = Q.front().f;
    int y = Q.front().s;
    //cerr << x << " " << y << " "<<D[x][y].f<<"\n";
    Q.pop();
    for (int i = 0; i < 5;i++)
    {
      int x2 = x + dx[i];
      int y2 = y + dy[i];
      p(x2);
      p(y2);
      if (!in(x2, H) || !in(y2, W) /* || grid[x2][y2] == '.' || grid[x2][y2] == '@'*/)
          continue;
      int x3 = x2;
      int y3=y2;
      if (rowDir[x2] == '>')
          y3 -= D[x][y].f;// t0;
      else if (rowDir[x2] == '<')
          y3 += D[x][y].f;// t0;
      p(x3);
      p(y3);
      if (in(x3, H) && in(y3, W) && grid[x3][y3] != '.' && grid[x3][y3] != '@')
      {
        if (rowDir[x2] == '>')
          y2++;
        else if(rowDir[x2]=='<')
          y2--;
        //if(in(y3, W))cerr <<" "<< x3 << " " << y3 <<" dir:"<<i<<" dist:"<<D[x][y].f+1<<" prev:"<<D[x3][y3].f<< " "<<x2<<","<<y2<<"\n";
        if(in(y2, W)  /*&& grid[x3][y3] != '.' && grid[x3][y3] != '@'*/ && D[x2][y2].f == -1)
        {
          Q.push(mp(x2, y2));
          D[x2][y2] = mp(D[x][y].f+1,i);
        }
      }
      /*if (cnt++ == 1000)
      {
        p(cnt);
        exit(1);
      }*/
    }
  }
  p("end bfs");
  return D;
}
int getDir(int ex,int ey,int sx,int sy)
{
  int x = ex,y=ey;
  int z;
  int c = 0;
  p(sx);
  p(sy);
  while (x != sx || y != sy)
  {
    p(x);
    p(y);
    z = D[x][y].s;
    p(z);

    if(rowDir[x]=='>')
      y--;
    else if(rowDir[x]=='<')
      y++;
    p(100*y);

    x -= dx[z];
    y -= dy[z];

    //p(10*x);
    //p(10*y);

    //if (c++ == 10)exit(1);
  }
  return z;
}
int main() 
{
  int F;
  int KL;
  int KW;
  double PC;
  cin >> H >> W >> F >> KL >> KW >> PC;

  for (int r=1; r<H; r++) cin >> rowDir[r];

  for (int turn=1; turn<=1000; turn++)
  {
    p(turn);
    for (int r = 0; r < H; r++)
    {
      for (int c=0; c<W; c++)
      {
        cin >> grid[r][c];
        //cerr<<grid[r][c];
      }
      //cerr<<"\n";
    }

    int elapsedTime;
    cin >> elapsedTime;

    vector<string> moves;

    for (int r=0; r<H; r++)
    {
      for (int c = 0; c < W; c++)
      {
        if (grid[r][c] == '@' && T[r][c]!=turn)
        {
          p(r);
          p(c);
          D = bfs(r, c,0);
          p(-r);
          p(-c);
          printGrid(D);
          int x2 = -1, y2;
          for (int x = 0; x < H; x++)
            for (int y = 0; y < W;y++)
            {
              int x3 = x;
              int y3=y;
              if(rowDir[x]=='>')
                y3 -= D[x][y].f;
              else if(rowDir[x]=='<')
                y3 += D[x][y].f;
              //p(D[x][y].f);
              //if(in(y3, W))cerr << x << " " << y << " " << x3 << " " << y3 <<" "<<grid[x3][y3]<< "\n";
              if (in(y3, W) && D[x][y].f != -1 && grid[x3][y3] == 'o')
              {
                //cerr << x << " " << y << " D:" << D[x][y].f << "\n";
                //if(x2 == -1 || D[x2][y2].f > D[x][y].f)//least moves
                if (x2 == -1 || x * D[x2][y2].f > x2 * D[x][y].f)//best profit per move
                {
                  vector<vector<pair<int, int>>> B = bfs(x, y, D[x][y].f);
                  int i = 0;
                  while(i<W && (grid[0][i]=='@' || B[0][i].f==-1))
                    i++;
                  p(i);
                  p(x);
                  p(y);
                  p(D[x][y].f);
                  printGrid(B);
                  if(i<W && turn+B[0][i].f<=1000)
                  {
                      p(turn + B[0][i].f);
                      //cerr << "found " + to_string(x) + "," + to_string(y) + " to 0," + to_string(i) << "\n";
                      x2 = x;
                      y2 = y;
                  }
                }
              }
            }
          p(0);
          p(x2);
          p(1);
          //p(y2);
          int z;
          if (x2 != -1)
          {
            p("if");
            z = getDir(x2, y2, r, c);
          }
          else if(r!=0)//return to beach
          {
            p("elseif");
            int i = -1;
            for (int j = 0; j < W;j++)
              if (D[0][j].f != -1)
                if(i==-1 || D[0][j].f<D[0][i].f)
                  i = j;
            p(i);
            if (i == -1)
              continue;
            z = getDir(0, i, r, c);
          }
          else
            continue;
          moves.pb(to_string(r) + " " + to_string(c) + " " + dir[z]);
          swap(grid[r][c], grid[r + dx[z]][c + dy[z]]);
          T[r + dx[z]][c + dy[z]] = turn;
        }
      }
    }
    if (T[0][0]!=turn && grid[0][0] == '@' && grid[0][1] == '@' && rowDir[1]=='<')//sacrifice if stuck in corner next to frog
        moves.pb("0 0 D");
    if (T[0][W-1]!=turn && grid[0][W-1] == '@' && grid[0][W-2] == '@' && rowDir[1]=='>')
        moves.pb("0 " + to_string(W-1)+ " D");

    p(moves.size());
    cout << moves.size() << endl;
    for (string &move : moves)
    {
      cout << move << endl;
      p(move);// cerr << move << endl;
    }
    cout.flush();
  }
  return 0;
}
